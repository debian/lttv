lttv.real(1) -- The Linux Trace Toolkit Viewer
==============================================

## SYNOPSIS

`lttv.real` [<options>]

## DESCRIPTION

`lttv.real` is a trace viewing tool for the new Linux Trace Toolkit trace
format.

## OPTIONS

  * `-h`, `--help`:
    Show summary of options.

  * `-m` &lt;MODULE&gt;:
    Load a module named MODULE.

  * `-L` &lt;DIR&gt;:
    Add the directory DIR to the module search path.

  * `-v`, `--verbose`:
    Print information messages.

  * `-d`, `--debug`:
    Print debugging messages.

  * `-e`, `--edebug`:
    Print event debugging.

  * `-f`, `--fatal`:
    Make critical messages fatal.

  * `-M`, `--memory`:
    Print memory information.

## SEE ALSO

lttv(1), lttv-gui(1).

## AUTHOR

`lttv.real` was written by Mathieu Desnoyers
&lt;mathieu.desnoyers@efficios.com&gt;.

This manual page was written by Jon Bernard &lt;jbernard@debian.org&gt;, for
the Debian project (and may be used by others).
