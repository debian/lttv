lttv(1) -- The Linux Trace Toolkit Viewer
=========================================

## DESCRIPTION

`lttv` is a simple script that starts lttv.real(1) with no modules. This is
useful for running lttv in batch mode.

## SEE ALSO

lttv-gui(1), lttv.real(1).

## AUTHOR

`lttv` was written by Mathieu Desnoyers &lt;mathieu.desnoyers@efficios.com&gt;.

This manual page was written by Jon Bernard &lt;jbernard@debian.org&gt;, for
the Debian project (and may be used by others).
