lttv-gui(1) -- The Linux Trace Toolkit Viewer
=============================================

## DESCRIPTION

`lttv-gui` is a simple script that starts lttv.real(1) with default GUI modules.

## SEE ALSO

lttv(1), lttv.real(1).

## AUTHOR

`lttv-gui` was written by Mathieu Desnoyers
&lt;mathieu.desnoyers@efficios.com&gt;.

This manual page was written by Jon Bernard &lt;jbernard@debian.org&gt;, for
the Debian project (and may be used by others).
